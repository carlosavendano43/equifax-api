const { Router } = require('express');
const router = Router();
const auth = require("../components/auth/auth.route");
const tickets = require("../components/tickets/tickets.route");

/**
 * @swagger
 * tags:
 *  - name: User
 *    description: User endpoint
 *  - name: Tickets
 *    description: Tickets endpoint
 */

/**
 * @swagger
 * components:
 *  schemas:
 *      User:
 *          type: object
 *          properties:
 *              email:
 *                  type: string
 *                  description: email del usuario
 *              password:
 *                  type: string
 *                  description: contraseña del usuario
 *          required:
 *              - email
 *              - password
 *          example:
 *              email: email@email.com
 *              password: contrasena
 * 
 *      UserBody:
 *          type: object
 *          properties:
 *              email:
 *                  type: string
 *                  description: email del usuario
 *              contrasena:
 *                  type: string
 *                  description: contraseña del usuario
 *          required:
 *             - email
 *             - contrasena
 *          example:
 *              email: email@email.com
 *              contrasena: contrasena
 * 
 *      responseOkUser:
 *            type: object
 *            properties:
 *              code: 
 *                type: number
 *              status: 
 *                type: string
 *              message:
 *                type: string
 *            example:
 *                code: 200
 *                status: ok
 *                message: mensaje response
 * 
 *      responseFailUser:
 *              type: object
 *              properties:
 *                  code:
 *                      type: number
 *                  status:
 *                      type: string
 *                  message:
 *                      type: string
 *              example:
 *                  code: 500
 *                  status: fail
 *                  message: mensaje response fail
 * 
 *      responsePasswordIncorrect:
 *              type: object
 *              properties:
 *                  code:
 *                      type: number
 *                  status:
 *                      type: string
 *                      description: codigo de respuesta
 *                  message:
 *                      type: string
 *                      description: mensaje
 *              example:
 *                  code: 400
 *                  status: fail
 *                  message: Contrasena incorrecta
 * 
 *      responseSignInOk:
 *              type: object
 *              properties:
 *                  code:
 *                      type: number
 *                      description: codigo de respuesta
 *                  status:
 *                      type: string
 *                      description: estado de la peticion
 *                  payload:
 *                      type: object
 *                      description: payload con el token
 *              example:
 *                  code: 200
 *                  status: ok
 *                  payload: { token: untoken }
 * 
 *      responseSignNoUser:
 *              type: object
 *              properties:
 *                  code:
 *                      type: number
 *                      description: codigo de respuesta
 *                  status:
 *                      type: string
 *                      description: estado de la peticion
 *                  message:
 *                      type: string
 *                      description: mensaje del error
 *              example:
 *                  code: 500
 *                  status: fail
 *                  message: Usuario no existe, por favor registrese
 * 
 */

/**
 * @swagger
 * /auth/sign-in:
 *  post:
 *      sumary: inicia el usuario
 *      tags: [User]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      $ref: '#/components/schemas/UserBody'
 *      responses:
 *           200:
 *              description: inicio de sesion exitoso
 *              content:
 *                  application/json:
 *                             schema:
 *                               $ref: '#/components/schemas/responseSignInOk'
 * 

 * 
 *           500:
 *              description: Usuario no existe
 *              content:
 *                  application/json:
 *                             schema:
 *                               $ref: '#/components/schemas/responseSignNoUser'
 *           400:
 *              description: Password Incorrecto
 *              content:
 *                  application/json:
 *                            schema:
 *                              $ref: '#/components/schemas/responsePasswordIncorrect'
 *                  
 */


/**
 * @swagger
 * /auth/sign-up:
 *  post:
 *      sumary: Registra usuario
 *      tags: [User]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      $ref: '#/components/schemas/UserBody'
 *      responses:
 *           200:
 *              description: registro con exito
 *              content:
 *                  application/json:
 *                             schema:
 *                               $ref: '#/components/schemas/responseOkUser'
 * 

 * 
 *           500:
 *              description: Usuario no existe
 *              content:
 *                  application/json:
 *                             schema:
 *                               $ref: '#/components/schemas/responseFailUser'
 *                  
 */

router.use('/auth', auth);


/**
 * @swagger
 * components:
 *      schemas:
 *          Tickets:
 *              type: object
 *              properties:
 *                  idUser:
 *                      type: string
 *                      description: id usuario
 *                  title:
 *                      type: string
 *                      description: titulo del tickets
 *                  description:
 *                      type: string
 *                      description: descripcion del tickets
 *                  status:
 *                      type: boolean
 *                      description: estado del tickects
 *              required:
 *                  - title
 *                  - description
 *                  - status
 *              example:
 *                  idUser: $2a$10$aHWbLYXE8qsrvRKMvKTX6uutq6i99EAdU7ADTKULK3773L9d5kxXS
 *                  title: titulo
 *                  description: descripcion
 *                  status: false
 * 
 *          TicketsBody:
 *              type: object
 *              properties:
 *                  title:
 *                      type: string
 *                      description: titulo del tickets
 *                  description:
 *                      type: string
 *                      description: descripcion del tickets
 *                  status:
 *                      type: boolean
 *                      description: estado del tickects
 *              required:
 *                  - title
 *                  - description
 *                  - status
 *              example:
 *                  title: titulo
 *                  description: descripcion
 *                  status: false
 * 
 *          responseOk:
 *            type: object
 *            properties:
 *                code: 
 *                    type: number
 *                status: 
 *                    type: string
 *                message:
 *                    type: string
 *            example:
 *                code: 200
 *                status: ok
 *                message: mensaje response
 * 
 *          responseFail:
 *            type: object
 *            properties:
 *                code:
 *                    type: number
 *                status:
 *                    type: string
 *                message:
 *                    type: string
 *            example:
 *                code: 500
 *                status: fail
 *                message: mensaje response fail
 * 
 * 
 *      parameters:
 *          ticketId:
 *              in: path
 *              name: id
 *              required: true
 *              schema:
 *                  type: string
 *                  description: parametro de id
 * 
 */


/**
 * @swagger
 * /tickets/delete/{id}:
 *  delete:
 *      sumary: elimina el ticket
 *      tags: [Tickets]
 *      parameters:
 *          - $ref: '#components/parameters/ticketId'
 *      responses:
 *           200:
 *              description: Eliminacion exitosa
 *              content:
 *                  application/json:
 *                              schema:
 *                                  $ref: '#/components/schemas/responseOk'
 *           500:
 *              description: Error al eliminar
 *              content:
 *                  application/json:
 *                              schema:
 *                                  $ref: '#/components/schemas/responseFail'
 *                  
 */


/**
 * @swagger
 * /tickets/register:
 *  post:
 *      sumary: Registra al usuario
 *      tags: [Tickets]
 *      requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                               schema:
 *                                 $ref:'#/components/schemas/TicketsBody'
 *      responses:
 *           200:
 *              description: Registro exitoso
 *              content:
 *                  application/json:
 *                             schema:
 *                               $ref: '#/components/schemas/responseOk'
 *           500:
 *              description: Error al registrar
 *              content:
 *                  application/json:
 *                             schema:
 *                                  $ref: '#/components/schemas/responseFail'
 *                  
 */


/**
 * @swagger
 * /tickets/modify:
 *  put:
 *      sumary: Modifica al usuario
 *      tags: [Tickets]
 *      requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                               schema:
 *                                 $ref:'#/components/schemas/TicketsBody'
 *      responses:
 *           200:
 *              description: Modificacion exitosa
 *              content:
 *                  application/json:
 *                             schema:
 *                               $ref: '#/components/schemas/responseOk'
 *           500:
 *              description: Error al modificar
 *              content:
 *                  application/json:
 *                             schema:
 *                                  $ref: '#/components/schemas/responseFail'
 *                  
 */

/**
 * @swagger
 * /tickets:
 *  get:
 *      sumary: Obtiene a todo los tickets
 *      tags: [Tickets]
 *      responses:
 *           200:
 *              description: Obtiene todos los usuarios
 *              content:
 *                  application/json:
 *                             schema:
 *                               type: object
 *                               properties:
 *                                      code: 
 *                                          type: number
 *                                      status: 
 *                                          type: string
 *                                      payload:
 *                                          type: object
 *                               example:
 *                                   code: 200
 *                                   status: ok
 *                                   payload: { data: [] }
 *           500:
 *              description: Error al obtener el usuario
 *              content:
 *                  application/json:
 *                             schema:
 *                                  $ref: '#/components/schemas/responseFail'
 *                  
 */

router.use('/tickets', tickets);

module.exports = router;