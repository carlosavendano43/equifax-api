const { getTokenBearer } = require("./jwt.helper");
const Tickets = require("../model/tickets.model");
exports.validaToken = (authorization) => {
    if(!authorization) throw {"status":"fail", "code":401, "message":"Usuario no authorizado"};
    const token = getTokenBearer(authorization);
    return token.id;
}

exports.validaTeUser = async(id,idUser) => {
    try {
        const r = await Tickets.findById(id).catch((e)=>{
            throw {"status":"fail","code":401,"message":"Usuario no identificado"}
        });
        if(r){
            if(r.idUser !== idUser) throw {"status":"fail", "code":401, "message":"Usuario no authorizado"};
        }else {
            throw {"status":"fail", "code":500, "message":"error de servidor"};
        }
    } catch(e) {
        throw {"status":"fail", "code":500, "message":"error de servidor"};
    }
}