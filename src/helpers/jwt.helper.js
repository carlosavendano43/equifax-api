const jwt = require('jsonwebtoken');

exports.getTokenBearer = (autorization) => {
    const t = autorization.split(" ");
    return jwt.decode(t[1]);
}

exports.decode = (token) => {
    return jwt.decode(token);
}

exports.verify = (token) => {
    return jwt.verify(token);
}

exports.sign = (data) => {
    return jwt.sign(
        data,
            process.env.JWT_SECRET,
        {
            expiresIn: process.env.JWT_EXPIRES_IN
        }
    )
}