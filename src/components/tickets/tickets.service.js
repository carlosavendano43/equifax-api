const { logger } = require("../../middleware/pino");
const Tickets = require("../../model/tickets.model");
const { validaToken, validaTeUser } = require("../../helpers/validateuser.helper");

class TicketsServices {

    static getTicket = async(req) => {
        logger.info(`TicketsServices getTicket:${JSON.stringify(req.headers)}`);
        try {
            let body = req.body;
            const idUser = validaToken(req.headers.authorization);
            let response;
            if(req.query.id) {
                const id = req.query.id;
                response = await Tickets.findOne({id});
                if(response.idUser !== idUser) throw {"status":"fail", "code":401, "message":"Usuario no corresponde"};
            }else {
                response = await  Tickets.find({idUser});
            }
        
            return { code:200, status:"ok", payload:{
                data: response
            }}
        } catch (error){
            logger.error(`TicketsServices getTicket error:${JSON.stringify(error)}`);
            const errors = !error.code ? {"status":"fail","code":500,"message":"error generico"}: error;
            throw errors;
        }
    }

    static putTicket = async(req) => {
        logger.info(`TicketsServices putTicket:${JSON.stringify(req.headers)}`);
        try {
            const {id, title, description, status } = req.body;
           
            const idUser = validaToken(req.headers.authorization);
   
            if(!idUser) throw {"status":"fail", "code":401, "message":"Usuario no authorizado"};
            const r = await Tickets.findById(id);
            if(r.idUser !== idUser) throw {"status":"fail", "code":401, "message":"Usuario no authorizado"};
            const response = await Tickets.findByIdAndUpdate(id, {title,description,status});
            if(response){
                return { code:200, status:"ok",message:"ticket modificado"};
            }else {
                throw {"status":"fail", "code":500, "message":"Error al modificar"};
            }
            
        } catch (error){
            logger.error(`TicketsServices putTicket error:${JSON.stringify(error)}`);
            const errors = !error.code ? {"status":"fail","code":500,"message":"error generico"}: error;
            throw error;
        }
    }

    static deleteTicket = async(req) => {
        logger.info(`TicketsServices deleteTicket:${JSON.stringify(req.headers)}`);
        try {
            const { id } = req.params;  
            const idUser = validaToken(req.headers.authorization);
            validaTeUser(id,idUser);
            const response = await Tickets.findByIdAndDelete(id).catch((e)=>{
                throw {"status":"fail","code":404,"message":"Tickets no eliminado"}
            });
            logger.info(`TicketsServices deleteTicket response:${JSON.stringify(response)}`);
            return { code:200, status:"ok",message:"ticket eliminado"}
        } catch (error){
            logger.error(`TicketsServices deleteTicket error:${JSON.stringify(error)}`);
            const errors = !error.code ? {"status":"fail","code":500,"message":"error generico"}: error;
            throw error;
        }
    }

    static postTicket = async(req) => {
        logger.info(`TicketsServices postTicket:${JSON.stringify(req.headers)}`);
        try {
            let body = req.body;
            const idUser = validaToken(req.headers.authorization);
            if(!idUser) throw {"status":"fail", "code":401, "message":"Usuario no authorizado"};
            body.idUser = idUser;
            const ticketsModel = new Tickets(body);
          
            const response = await ticketsModel.save();
          
           return {"status":"ok","code":200,"message":"registro exitoso"};
        } catch (error){
            logger.error(`TicketsServices postTicket error:${JSON.stringify(error)}`);
            const errors = !error.code ? {"status":"fail","code":500,"message":"error generico"}: error;
            throw error;
        }
    }
}

module.exports = TicketsServices;