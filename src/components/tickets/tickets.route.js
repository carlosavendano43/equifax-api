const { Router } = require('express');
const router = Router();
const TicketsController = require("./tickets.controller");

router.get('/',TicketsController.getTicket);
router.post('/register',TicketsController.postTicket);
router.put('/modify',TicketsController.putTicket);
router.delete('/delete/:id',TicketsController.deleteTicket);


module.exports = router;