const { logger } = require("../../middleware/pino");
const TicketsServices = require("./tickets.service");
class TicketsController {
    static getTicket = async(req,res) => {
        try {
            logger.info('TicketsController:getTicket')
            const response = await TicketsServices.getTicket(req);
            res.status(response.code).json(response); 
        }catch (e){
            logger.error(`TicketsController getTicket error:${e}`);
            res.status(e.code).json(e);
        }
    }

    static putTicket = async(req,res) => {
        try {
            logger.info('TicketsController:putTicket')
            const response = await TicketsServices.putTicket(req);
            res.status(response.code).json(response); 
        }catch (e){
            logger.error(`TicketsController putTicket error:${e}`);
            res.status(e.code).json(e);
        }
    }

    static deleteTicket = async(req,res) => {
        try {
            logger.info('TicketsController:deleteTicket')
            const response = await TicketsServices.deleteTicket(req);
            res.status(response.code).json(response); 
        }catch (e){
            logger.error(`TicketsController deleteTicket error:${e}`);
            res.status(e.code).json(e);
        }
    }

    static postTicket = async(req,res) => {
        try {
            logger.info('TicketsController:postTicket')
            const response = await TicketsServices.postTicket(req);
            res.status(response.code).json(response); 
        }catch (e){
            logger.error(`TicketsController postTicket error:${e}`);
            res.status(e.code).json(e);
        }
    }
}

module.exports = TicketsController;