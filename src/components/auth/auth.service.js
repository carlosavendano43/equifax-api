
const User = require("../../model/user.model");
const jwt = require("jsonwebtoken");
const { hashPassword, checkPassword } = require("../../helpers/bcrypt.helper");
const { logger } = require("../../middleware/pino");
class AuthServices {
    static SignIn = async(req) => {
        logger.info('AuthService SignIn call');
        try {
            const { email, contrasena } = req.body;
    
            if(!(email && contrasena)) throw {status:"fail",code:500,message: "email y password requerido"}
            
            const user = await User.findOne({email});
            if(!user) throw {status:"fail",code:500,message: "Usuario no existe, por favor registrese"};
            
            const check = checkPassword(contrasena,user.password);
      
            if(!check) throw {status:"fail",code:400,message: "Contraseña incorrecta"};

            const token = jwt.sign(
                {
                    id:user.id,
                    email:user.email
                },
                    process.env.JWT_SECRET,
                {
                    expiresIn: process.env.JWT_EXPIRES_IN
                }
            )
            
            return {status:"ok",code:200,payload:{token}};
        } catch (error){
            logger.error(`AuthService SignIn error:${JSON.stringify(error)}`);
            return error;
        }
    }

    static SignUp = async(req) => {
        logger.info('AuthService SignUp call');
        try {
            const { email, contrasena } = req.body;
            if(!(email && contrasena)) throw {code:500, status:"fail",message: "email y password requerido"}
            const user = await User.findOne({email})
            
            if(user) throw {status:"fail",code:500,message:"email existente"}
            const password = hashPassword(contrasena);

            const userModel = new User({email,password});

            await userModel.save();

            return {status:"ok",code:200,message:"registro exitoso"};
        } catch (error){
            logger.error(`AuthService SignUp error:${JSON.stringify(error)}`);
            throw error;
        }
    }
}

module.exports = AuthServices;