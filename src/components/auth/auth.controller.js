const AuthServices = require("./auth.service");
const { logger } = require("../../middleware/pino");
class AuthController {
    static SignIn = async(req,res) => {
        try {
            logger.info('AuthController:SignIn')
            const response = await AuthServices.SignIn(req);
            res.status(response.code).json(response); 
        }catch (e){
            logger.error(`AuthController signIn error:${e}`);
            res.status(e.code).json(e);
        }
    }

    static SignUp = async(req,res) => {
        try {
            logger.info('AuthController:SignUP');
            const response = await AuthServices.SignUp(req);
            res.status(response.code).json(response); 
        }catch (e){
            logger.info(`AuthController SignUP error:${e}`);
            res.status(e.code).json(e);
        }
    }
}

module.exports = AuthController;