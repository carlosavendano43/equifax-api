const { Router } = require('express');
const router = Router();

const AuthController  = require('./auth.controller');

router.post('/sign-in',AuthController.SignIn);
router.post('/sign-up',AuthController.SignUp);

module.exports = router;