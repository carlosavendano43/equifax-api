const express = require('express');
const app = express();
const cors = require('cors');
const Port = process.env.PORT || 3000;
const routes = require("./routes/index.router");
const path = require("path");
require('dotenv').config({path:'./variable.env'});
////// swagger //////
const swaggerUI =  require("swagger-ui-express");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerSpec = {
    definition: {
        openapi:"3.0.0",
        info:{
            title: "Equifax prueba API",
            version: "1.0.0"
        },
        servers: [{
            url: "http://localhost:3000"
        }]
    },
    apis: [`${path.join(__dirname, "./routes/*.js")}`]
}
///// database //////
const connectDB = require ('./config/database');
connectDB();
///// middlewares ////
app.use(cors());
app.use(express.json({extended:true}));
app.use("/api-doc", swaggerUI.serve, swaggerUI.setup(swaggerJsDoc(swaggerSpec)));
//// router ////
app.use(routes);

app.listen(Port,()=>{
    console.log('Servidor iniciado',Port);
})