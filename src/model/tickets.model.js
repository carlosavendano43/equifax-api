const { Schema, model } = require('mongoose');

const ticketsSchema = new Schema({
    idUser:{
        type: String,
        trim: true,
        required: true,
        lowercase: true
    },
    title:{
        type: String,
        required: true,
    },
    description:{
        type: String,
        required: true,
    },
    status:{
        type: Boolean,
        required: true,
    },
},{
    timestamps:true,
})


module.exports = model('Tickets',ticketsSchema);